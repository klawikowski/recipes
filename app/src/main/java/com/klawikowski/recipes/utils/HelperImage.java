package com.klawikowski.recipes.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.klawikowski.recipes.model.Recipe;

/**
 * Class with all tools / helper methods for image operations
 * WARNING!!
 * https://github.com/bumptech/glide/issues/743
 */
public class HelperImage
{
    /**
     * Loads image from recipe.
     * ## NULLPOINTER SOLID!
     * # check code
     *
     * @param pContext   Context
     * @param pRecipe    Recipe
     * @param pImageView Target imageview
     */
    public static void loadImageFromRecipeData(Context pContext, Recipe pRecipe, ImageView pImageView)
    {
        try
        {
            //ok this is not nice ... but its really working ; ) if we cant get image - PUFF, nothing happens white placeholder :D
            //TODO: mby better gfx ?
            Glide.with(pContext).load(pRecipe.getImages().get(0).getUrl()).placeholder(new ColorDrawable(Color.WHITE)).into(pImageView);
        }
        catch(Exception e)
        {
            //hmmm :<
            e.printStackTrace();
        }

    }
}
