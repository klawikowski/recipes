package com.klawikowski.recipes.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.klawikowski.recipes.utils.HelperBus;
import com.klawikowski.recipes.utils.HelperConnection;

/**
 * Broadcast receiver for connection changes
 */
public class ConnectionChangeReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        HelperBus.getInstance().post(new ConnectionStatus(HelperConnection.isOnline(context)));
    }
}
