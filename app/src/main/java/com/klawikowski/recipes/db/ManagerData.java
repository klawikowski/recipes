package com.klawikowski.recipes.db;

import android.content.Context;

import com.klawikowski.recipes.model.Recipe;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Manager Data class
 * Contains all data crud logic
 */
public class ManagerData implements IManagerData
{
    private static ManagerData sInstance;

    /**
     * Returns singleton instance of Manager Data
     *
     * @param pContext Context
     * @return singleton instance
     */
    public static ManagerData getInstance(Context pContext)
    {
        if(sInstance == null)
        {
            synchronized(ManagerData.class)
            {
                if(sInstance == null)
                {
                    sInstance = new ManagerData(pContext);
                }
            }
        }
        return sInstance;
    }

    /**
     * Private constructor
     */
    private ManagerData(Context pContext)
    {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(pContext).build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    /**
     * Adds recipes to database
     *
     * @param pRecipes
     */
    @Override
    public void addRecipes(ArrayList<Recipe> pRecipes)
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(pRecipes);
        realm.commitTransaction();

    }

    /**
     * Returns all recipes stored in db
     *
     * @return recipes stored in db
     */
    @Override
    public ArrayList<Recipe> getRecipes()
    {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<Recipe> result = new ArrayList<>();
        result.addAll(realm.copyFromRealm(realm.where(Recipe.class).findAll()));
        return result;
    }

    /**
     * Returns recipe with specified id
     *
     * @param pRecipeId recipe id
     * @return THIS recipe
     */
    @Override
    public Recipe getRecipe(int pRecipeId)
    {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Recipe.class).equalTo("id", pRecipeId).findFirst();
    }
}
