package com.klawikowski.recipes.db;

import com.klawikowski.recipes.model.Recipe;

import java.util.ArrayList;

/**
 * Adapter interface for ManagerData
 */
public interface IManagerData
{
    /**
     * Adds recipes to database
     *
     * @param pRecipes
     */
    void addRecipes(ArrayList<Recipe> pRecipes);

    /**
     * Returns all recipes stored in db
     *
     * @return recipes stored in db
     */
    ArrayList<Recipe> getRecipes();

    /**
     * Returns recipe with specified id
     *
     * @param pRecipeId recipe id
     * @return Recipe
     */
    Recipe getRecipe(int pRecipeId);
}
