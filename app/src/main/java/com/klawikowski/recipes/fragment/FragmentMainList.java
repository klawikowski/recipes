package com.klawikowski.recipes.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.klawikowski.recipes.R;
import com.klawikowski.recipes.RecipesApplication;
import com.klawikowski.recipes.activity.ActivityMain;
import com.klawikowski.recipes.activity.ActivityRecipeDetails;
import com.klawikowski.recipes.adapter.AdapterMainList;
import com.klawikowski.recipes.model.Recipe;
import com.klawikowski.recipes.receivers.ConnectionStatus;
import com.klawikowski.recipes.utils.HelperBus;
import com.klawikowski.recipes.utils.HelperConnection;
import com.klawikowski.recipes.ws.IRetrofitAdapter;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment presenting list of recipes in {@link ActivityMain}
 */
public class FragmentMainList extends FragmentBase implements AdapterMainList.ListClickListener
{
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private AdapterMainList mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String mQuery = "";
    private LinearLayout mNoConnectionBar;

    private int paginationIndex = 0;

    @Override
    public int getLayoutId()
    {
        return R.layout.fragment_main_list;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        //needed to add options actions in toolbar
        setHasOptionsMenu(true);
        HelperBus.getInstance().register(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        MenuItem searchItem = menu.findItem(R.id.menu_main_search);
        SearchView searchView = new SearchView(((ActivityMain) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(searchItem, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(searchItem, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                if(mAdapter != null)
                {
                    mAdapter.filter(query);
                }
                mQuery = query;
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                if(mAdapter != null)
                {
                    mAdapter.filter(newText);
                }
                mQuery = newText;
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void setupView(View pFragmentView)
    {
        mNoConnectionBar = (LinearLayout) pFragmentView.findViewById(R.id.fragment_main_list_wrapper_no_connection);
        mSwipeRefreshLayout = (SwipeRefreshLayout) pFragmentView.findViewById(R.id.fragment_main_list_recycler_swipe_refresh_layout);
        mSwipeRefreshLayout.setEnabled(false);
        mRecyclerView = (RecyclerView) pFragmentView.findViewById(R.id.fragment_main_list_recycler);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                //if search is disabled
                if(mQuery.length() == 0)
                {
                    //and scrolling down
                    if(dy > 0)
                    {
                        int visibleItemCount = mLayoutManager.getChildCount();
                        int totalItemCount = mLayoutManager.getItemCount();
                        int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                        //almost last visible cell from bottom
                        if((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            getWsData(true);
                        }
                    }
                }
            }
        });


        mRecyclerView.setLayoutManager(mLayoutManager);
        ArrayList<Recipe> recipes = new ArrayList<>(RecipesApplication.getManagerData(getContext()).getRecipes());
        mAdapter = new AdapterMainList(getContext(), recipes, this);
        mRecyclerView.setAdapter(mAdapter);

        changeConnectionStatusBarVisibility(HelperConnection.isOnline(getContext()));
        getWsData(false);
    }

    /**
     * Basing on connection status from {@link HelperConnection} hides / shows bar indicating no connections status
     */
    private void changeConnectionStatusBarVisibility(boolean pIsConnectionAvailable)
    {
        mNoConnectionBar.setVisibility(pIsConnectionAvailable ? View.GONE : View.VISIBLE);
    }

    /**
     * Shows loading indicator
     * Issue Solution : //http://stackoverflow.com/questions/26858692/swiperefreshlayout-setrefreshing-not-showing-indicator-initially
     */
    private void showProgress()
    {

        mSwipeRefreshLayout.post(new Runnable()
        {
            @Override
            public void run()
            {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    /**
     * Gets another portion of data from WS
     *
     * @param pPaginateLoad true if load should paginate next page
     */
    private void getWsData(boolean pPaginateLoad)
    {
        if(HelperConnection.isOnline(getContext()))
        {
            if(pPaginateLoad)
            {
                paginationIndex++;
            }
            showProgress();
            RecipesApplication.getManagerWs().getRecipes(paginationIndex, new IRetrofitAdapter.onRetrofitResponseCallback<List<Recipe>>()
            {
                @Override
                public void onSuccess(List<Recipe> pReturn)
                {
                    RecipesApplication.getManagerData(getContext()).addRecipes((ArrayList<Recipe>) pReturn);
                    //disable refresh indicator
                    mSwipeRefreshLayout.setRefreshing(false);
                    //add data and refresh the list
                    if(pReturn != null)
                    {
                        mAdapter.addData((ArrayList<Recipe>) pReturn);
                    }
                }

                @Override
                public void onError()
                {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    @Override
    public void onListItemClick(int pPosition, Recipe pRecipe, View pView)
    {
        Intent recipeDetailIntent = new Intent(getContext(), ActivityRecipeDetails.class);
        //passing recipe id so in details we can find it in db
        recipeDetailIntent.putExtra(ActivityRecipeDetails.BUDLE_RECIPE_ID, pRecipe.getId());

        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(getActivity(), pView.findViewById(R.id.cell_list_recipe_thumbnail), getString(R.string.STRING_TRANSITION_LIST_TO_DETAILS));
        startActivity(recipeDetailIntent, options.toBundle());
    }

    @Subscribe
    public void connectionChanged(ConnectionStatus pConnectionStatus)
    {
        changeConnectionStatusBarVisibility(pConnectionStatus.isConnected());

        //this mean its a fresh start and user has no connection
        if(mAdapter.getItemCount() == 0)
        {
            paginationIndex = 0;
            getWsData(false);
        }
    }
}
