package com.klawikowski.recipes.ws;

import com.klawikowski.recipes.model.Recipe;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;


/**
 * Retrofit Endpoint Api
 */
public interface IRetrofitAdapter
{
    /**
     * Callback interface
     *
     * @param <T> Type of return
     */
    interface onRetrofitResponseCallback<T>
    {
        void onSuccess(T pReturn);

        void onError();
    }

    @GET("/getRecipesListDetailed?tags=&size=thumbnail-medium&ratio=1")
    void getRecipes(@Query("limit") int pLimit, @Query("from") int pFrom, Callback<List<Recipe>> pCallback);
}
