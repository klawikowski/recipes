package com.klawikowski.recipes.ws;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.klawikowski.recipes.BuildConfig;
import com.klawikowski.recipes.model.Recipe;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.realm.RealmObject;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Webservice Manager Class
 * Contains all logic connected with webservice actions
 */
public class ManagerWS implements IWSCalls
{
    //singleton instance
    private static ManagerWS sInstance;

    //retrofit adapter
    private IRetrofitAdapter mApiEndpoint;

    //http client
    private RecipesClient mClient;

    /**
     * Returns singleton instance
     *
     * @return singleton instance
     */
    public static ManagerWS getInstance()
    {
        if(sInstance == null)
        {
            synchronized(ManagerWS.class)
            {
                if(sInstance == null)
                {
                    sInstance = new ManagerWS();
                }
            }
        }
        return sInstance;
    }

    /**
     * Private constructor
     */
    private ManagerWS()
    {
        //client init
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        //wrapping client into our dedicated client
        mClient = new RecipesClient(okHttpClient);

        // GSON can parse the data.
        // Note there is a bug in GSON 2.5 that can cause it to StackOverflow when working with RealmObjects.
        // To work around this, use the ExclusionStrategy below or downgrade to 1.7.1
        // See more here: https://code.google.com/p/google-gson/issues/detail?id=440
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy()
                {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f)
                    {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz)
                    {
                        return false;
                    }
                })
                .create();

        //creating adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(EndpointConfig.RECIPES_BASE_ENDPOINT)
                .setClient(mClient)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();

        //init
        mApiEndpoint = restAdapter.create(IRetrofitAdapter.class);
    }

    @Override
    public void getRecipes(int pPaginationOffset, final IRetrofitAdapter.onRetrofitResponseCallback<List<Recipe>> pCallback)
    {
        mApiEndpoint.getRecipes(getNumberOfRecipesPerPaginationPage(), pPaginationOffset, new Callback<List<Recipe>>()
        {
            @Override
            public void success(List<Recipe> pRecipes, Response response)
            {
                pCallback.onSuccess(pRecipes);
            }

            @Override
            public void failure(RetrofitError error)
            {
                pCallback.onError();
            }
        });
    }

    @Override
    public int getNumberOfRecipesPerPaginationPage()
    {
        return EndpointConfig.NUMBER_OF_RECIPES_PER_PAGINATION_PAGE;
    }
}
