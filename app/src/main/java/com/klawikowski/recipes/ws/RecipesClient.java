package com.klawikowski.recipes.ws;

import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;

import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;

/**
 * Http Client wrapper
 */
public class RecipesClient extends OkClient
{
    @Override
    public Response execute(Request request) throws IOException
    {
        return super.execute(request);
    }

    public RecipesClient(OkHttpClient pOkHttpClient)
    {
        super(pOkHttpClient);
    }
}
