package com.klawikowski.recipes.ws;

import com.klawikowski.recipes.model.Recipe;

import java.util.List;

/**
 * Simple interface describing all possible calls to WS
 */
public interface IWSCalls
{
    /**
     * Invokes GET call to endpoint
     *
     * @param pPaginationOffset Pagination offset
     * @param pCallback         Callback interface
     */
    public void getRecipes(int pPaginationOffset, IRetrofitAdapter.onRetrofitResponseCallback<List<Recipe>> pCallback);

    /**
     * @return Number of recipes loaded per each pagination request
     */
    public int getNumberOfRecipesPerPaginationPage();

}
