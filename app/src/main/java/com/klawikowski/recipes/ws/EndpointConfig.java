package com.klawikowski.recipes.ws;

/**
 * Class containing all constats for endpoints used in application
 */
public class EndpointConfig
{
    //    public static final String RECIPES_BASE_ENDPOINT = "http://www.godt.no/api/getRecipesListDetailed?tags=&size=thumbnail-medium&ratio=1&limit=50&from=0";
    public static final String RECIPES_BASE_ENDPOINT = "http://www.godt.no/api/";

    public static int NUMBER_OF_RECIPES_PER_PAGINATION_PAGE = 50;
}
