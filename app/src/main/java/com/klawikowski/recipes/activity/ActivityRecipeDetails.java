package com.klawikowski.recipes.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.klawikowski.recipes.R;
import com.klawikowski.recipes.RecipesApplication;
import com.klawikowski.recipes.model.Recipe;
import com.klawikowski.recipes.model.recipe.Element;
import com.klawikowski.recipes.model.recipe.Ingredient;
import com.klawikowski.recipes.utils.HelperImage;

/**
 * Recipe detail activity
 */
public class ActivityRecipeDetails extends AppCompatActivity
{
    public static final String BUDLE_RECIPE_ID = "BUNDLE_RECIPE_ID";

    private Toolbar mToolbar;
    private Recipe mRecipe;

    private ImageView mMainImage;
    private TextView mTitle;
    private TextView mDescription;
    private TextView mIngredients;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);

        mToolbar = (Toolbar) findViewById(R.id.activity_recipe_details_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mMainImage = (ImageView) findViewById(R.id.activity_recipe_details_image);
        mTitle = (TextView) findViewById(R.id.activity_recipe_details_title);
        mDescription = (TextView) findViewById(R.id.activity_recipe_details_description);
        mIngredients = (TextView) findViewById(R.id.activity_recipe_details_ingredients);

        //check for passed data from fragment list click
        if(getIntent().getExtras() != null)
        {
            mRecipe = RecipesApplication.getManagerData(getApplicationContext()).getRecipe(getIntent().getIntExtra(BUDLE_RECIPE_ID, 0));
        }

        if(mRecipe != null)
        {
            fillViewWithRecipeData(mRecipe);
        }
    }

    /**
     * Fills view with recipe data
     *
     * @param pRecipe Recipe
     */
    private void fillViewWithRecipeData(Recipe pRecipe)
    {
        if(mRecipe != null)
        {
            getSupportActionBar().setTitle(mRecipe.getTitle());

            HelperImage.loadImageFromRecipeData(this, pRecipe, mMainImage);

            mTitle.setText(mRecipe.getTitle());
            mDescription.setText(Html.fromHtml(mRecipe.getDescription()));

            StringBuilder fullIngredients = new StringBuilder();

            for(Ingredient ingredient : mRecipe.getIngredients())
            {
                fullIngredients.append("\n");
                fullIngredients.append("\n");
                fullIngredients.append("Ingredient ");
                fullIngredients.append("\n");
                fullIngredients.append(ingredient.getName());
                fullIngredients.append("\n");
                fullIngredients.append("Elements");
                fullIngredients.append("\n");
                for(Element element : ingredient.getElements())
                {
                    fullIngredients.append(element.getName());
                    fullIngredients.append("amount ");
                    fullIngredients.append(element.getAmount());
                    fullIngredients.append(" ");
                    fullIngredients.append(element.getUnitName());
                    fullIngredients.append("\n");
                }
            }

            mIngredients.setText(fullIngredients.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            supportFinishAfterTransition(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
