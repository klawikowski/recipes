package com.klawikowski.recipes.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.klawikowski.recipes.R;

/**
 * ViewHolder for Recipe List Cell used in {@link com.klawikowski.recipes.adapter.AdapterMainList}
 */
public class ViewHolderRecipe extends RecyclerView.ViewHolder
{
    private TextView mTextTitle;
    private ImageView mThumbnail;
    private TextView mTextDescription;
    private View mCellView;

    public ViewHolderRecipe(View itemView)
    {
        super(itemView);
        mCellView = itemView;
        mTextTitle = (TextView) itemView.findViewById(R.id.cell_recipe_list_title);
        mThumbnail = (ImageView) itemView.findViewById(R.id.cell_list_recipe_thumbnail);
        mTextDescription = (TextView) itemView.findViewById(R.id.cell_recipe_list_description);
    }

    /**
     * @return TextView for Description
     */
    public TextView getDescription()
    {
        return mTextDescription;
    }

    /**
     * @return TextView for Title
     */
    public TextView getTitle()
    {
        return mTextTitle;
    }

    /**
     * @return ImageView for thumbnail
     */
    public ImageView getThumbnail()
    {
        return mThumbnail;
    }

    /**
     * @return View of cell
     */
    public View getCellView()
    {
        return mCellView;
    }
}
