package com.klawikowski.recipes.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.klawikowski.recipes.R;
import com.klawikowski.recipes.adapter.filter.AdapterMainListFilter;
import com.klawikowski.recipes.adapter.viewholder.ViewHolderRecipe;
import com.klawikowski.recipes.model.Recipe;
import com.klawikowski.recipes.utils.HelperImage;

import java.util.ArrayList;

/**
 * Recycler Adapter for {@link com.klawikowski.recipes.fragment.FragmentMainList} fragment
 */
public class AdapterMainList extends RecyclerView.Adapter<ViewHolderRecipe> implements Filterable
{
    //original data set
    private ArrayList<Recipe> mData = new ArrayList<>();
    //filtered data set
    private ArrayList<Recipe> mFilteredData = new ArrayList<>();
    private Context mContext;
    private ListClickListener mListener;

    /**
     * Public contstructor
     * @param pContext Context
     * @param pData Collection of recipes
     * @param pListener List click listener
     */
    public AdapterMainList(Context pContext, ArrayList<Recipe> pData, ListClickListener pListener)
    {
        mContext = pContext;
        if(pListener != null)
        {
            mListener = pListener;
        }
        if(pData != null)
        {
            //adding data to all collections
            mData.addAll(pData);
            mFilteredData.addAll(pData);
        }
    }

    /**
     * Filters list content on specified query.
     * ## CHECK filter documentation which fields from recipe are filtered
     * @param pQuery
     */
    public void filter(String pQuery)
    {
        getFilter().filter(pQuery);
    }

    /**
     * Adds data to current one.
     * It will add it at the last position.
     * Used for pagination load.
     *
     * @param pPaginationData Paginated data to load
     */
    public void addData(ArrayList<Recipe> pPaginationData)
    {
        if(pPaginationData != null)
        {
            mData.addAll(pPaginationData);
            mFilteredData.addAll(pPaginationData);
            notifyDataSetChanged();
        }
    }

    /**
     * Sets new data.
     * It will fully clear the data and set given one.
     * Used for filter action.
     *
     * @param pFreshData
     */
    public void setData(ArrayList<Recipe> pFreshData)
    {
        if(pFreshData != null)
        {
            //clearing data - setting new one
            mFilteredData.clear();
            mFilteredData.addAll(pFreshData);
            notifyDataSetChanged();
        }

    }


    @Override
    public int getItemViewType(int position)
    {
        //TODO: add enum with empty type to show proper info when refreshing / error
        //http://stackoverflow.com/questions/27414173/equivalent-of-listview-setemptyview-in-recyclerview
        //https://gist.github.com/adelnizamutdinov/31c8f054d1af4588dc5c
        return super.getItemViewType(position);
    }

    @Override
    public ViewHolderRecipe onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View cellView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_recipe_list, parent, false);
        ViewHolderRecipe viewHolderRecipe = new ViewHolderRecipe(cellView);
        return viewHolderRecipe;
    }

    @Override
    public void onBindViewHolder(ViewHolderRecipe holder, final int position)
    {
        //simple check to avoid nullchecks
        if(mFilteredData != null && mFilteredData.size() > 0)
        {
            final Recipe recipe = mFilteredData.get(position);
            //sometimes we get <br /> etc in title and description - this handles all strange codes that might occur
            holder.getTitle().setText(Html.fromHtml(recipe.getTitle()));
            holder.getDescription().setText(Html.fromHtml(recipe.getDescription()));
            // auto check inside loadImageFromRecipeData method
            HelperImage.loadImageFromRecipeData(mContext, recipe, holder.getThumbnail());

            holder.getCellView().setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mListener != null)
                    {
                        mListener.onListItemClick(position, recipe, v);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount()
    {
        return mFilteredData.size();
    }

    @Override
    public Filter getFilter()
    {
        return new AdapterMainListFilter(this, mData);
    }

    /**
     * Listener for list clicks
     */
    public interface ListClickListener
    {
        void onListItemClick(int pPosition, Recipe pRecipe, View pView);
    }
}
