package com.klawikowski.recipes.adapter.filter;

import android.widget.Filter;

import com.klawikowski.recipes.adapter.AdapterMainList;
import com.klawikowski.recipes.model.Recipe;
import com.klawikowski.recipes.model.recipe.Ingredient;

import java.util.ArrayList;

/**
 * Filter class for {@link com.klawikowski.recipes.adapter.AdapterMainList}
 */
public class AdapterMainListFilter extends Filter
{
    // min count of characters to start search
    private static final int MIN_CHARACTERS_TO_START = 1;

    private ArrayList<Recipe> mDataFiltered = new ArrayList<>();
    private ArrayList<Recipe> mData = new ArrayList<>();
    private AdapterMainList mAdapterMainList;

    /**
     * Constructor
     *
     * @param pAdapter Adapter connected to Filter class
     * @param pData    Full data set to filter
     */
    public AdapterMainListFilter(AdapterMainList pAdapter, ArrayList<Recipe> pData)
    {
        mAdapterMainList = pAdapter;
        mData = new ArrayList<>(pData);
    }

    /**
     * Performs filter action
     *
     * FILTERED FIELDS:
     * TITLE {@code Recipe.getTitle}
     * INGREDIENTS {@code Recipe.getIngredients}
     *
     * @param constraint
     * @return filtered data
     */
    @Override
    protected FilterResults performFiltering(CharSequence constraint)
    {
        FilterResults filterResults = new FilterResults();
        //if no letters - show original data
        if(constraint.toString().length() < MIN_CHARACTERS_TO_START)
        {
            filterResults.values = mData;
            filterResults.count = mData.size();
        }
        //1 letter or more - lets search!
        else
        {
            mDataFiltered.clear();
            //this will make it more reliable and user "dumb" protected :)
            String constraintRaw = constraint.toString().toLowerCase().trim();

            for(Recipe recipe : mData)
            {
                //first step - title
                if(recipe.getTitle().toLowerCase().contains(constraintRaw))
                {
                    mDataFiltered.add(recipe);
                }
                //second step - ingredients
                else
                {
                    for(Ingredient ingredient : recipe.getIngredients())
                    {
                        if(ingredient.getName().toLowerCase().contains(constraint))
                        {
                            mDataFiltered.add(recipe);
                            //ok we found matching data - no need to loop more
                            //thanks to that we dont get doubled recipes because ingredientS match
                            continue;
                        }
                    }
                }
            }
            filterResults.values = mDataFiltered;
            filterResults.count = mDataFiltered.size();
        }
        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        //passing found data in performFiltering() method
        mAdapterMainList.setData((ArrayList<Recipe>) results.values);
    }
}
