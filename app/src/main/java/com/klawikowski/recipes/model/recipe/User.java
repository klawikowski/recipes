
package com.klawikowski.recipes.model.recipe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class User extends RealmObject
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("receiveNotifications")
    @Expose
    private Integer receiveNotifications;
    @SerializedName("role")
    @Expose
    private String role;

    /**
     * @return The id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return The displayName
     */
    public String getDisplayName()
    {
        return displayName;
    }

    /**
     * @param displayName The displayName
     */
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    /**
     * @return The image
     */
    public String getImage()
    {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image)
    {
        this.image = image;
    }

    /**
     * @return The email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * @return The receiveNotifications
     */
    public Integer getReceiveNotifications()
    {
        return receiveNotifications;
    }

    /**
     * @param receiveNotifications The receiveNotifications
     */
    public void setReceiveNotifications(Integer receiveNotifications)
    {
        this.receiveNotifications = receiveNotifications;
    }

    /**
     * @return The role
     */
    public String getRole()
    {
        return role;
    }

    /**
     * @param role The role
     */
    public void setRole(String role)
    {
        this.role = role;
    }

}
