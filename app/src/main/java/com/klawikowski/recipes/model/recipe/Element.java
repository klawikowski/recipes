package com.klawikowski.recipes.model.recipe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;

public class Element extends RealmObject
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @Ignore
    @SerializedName("hint")
    @Expose
    private Object hint;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("unitName")
    @Expose
    private String unitName;
    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("menyCategory")
    @Expose
    private MenyCategory menyCategory;

    /**
     * @return The id
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * @return The amount
     */
    public Double getAmount()
    {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(Double amount)
    {
        this.amount = amount;
    }

    /**
     * @return The hint
     */
    public Object getHint()
    {
        return hint;
    }

    /**
     * @param hint The hint
     */
    public void setHint(Object hint)
    {
        this.hint = hint;
    }

    /**
     * @return The name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return The unitName
     */
    public String getUnitName()
    {
        return unitName;
    }

    /**
     * @param unitName The unitName
     */
    public void setUnitName(String unitName)
    {
        this.unitName = unitName;
    }

    /**
     * @return The symbol
     */
    public String getSymbol()
    {
        return symbol;
    }

    /**
     * @param symbol The symbol
     */
    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }

    /**
     * @return The menyCategory
     */
    public MenyCategory getMenyCategory()
    {
        return menyCategory;
    }

    /**
     * @param menyCategory The menyCategory
     */
    public void setMenyCategory(MenyCategory menyCategory)
    {
        this.menyCategory = menyCategory;
    }

}
