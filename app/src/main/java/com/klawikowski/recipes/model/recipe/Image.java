
package com.klawikowski.recipes.model.recipe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Image extends RealmObject
{

    @SerializedName("imboId")
    @Expose
    private String imboId;
    @SerializedName("url")
    @Expose
    private String url;

    /**
     * @return The imboId
     */
    public String getImboId()
    {
        return imboId;
    }

    /**
     * @param imboId The imboId
     */
    public void setImboId(String imboId)
    {
        this.imboId = imboId;
    }

    /**
     * @return The url
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

}
