
package com.klawikowski.recipes.model.recipe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;

public class Step extends RealmObject
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @Ignore
    @SerializedName("heading")
    @Expose
    private Object heading;
    @SerializedName("description")
    @Expose
    private String description;
    @Ignore
    @SerializedName("image")
    @Expose
    private Object image;
    @Ignore
    @SerializedName("video")
    @Expose
    private Object video;

    /**
     * @return The id
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * @return The heading
     */
    public Object getHeading()
    {
        return heading;
    }

    /**
     * @param heading The heading
     */
    public void setHeading(Object heading)
    {
        this.heading = heading;
    }

    /**
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return The image
     */
    public Object getImage()
    {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(Object image)
    {
        this.image = image;
    }

    /**
     * @return The video
     */
    public Object getVideo()
    {
        return video;
    }

    /**
     * @param video The video
     */
    public void setVideo(Object video)
    {
        this.video = video;
    }

}
