
package com.klawikowski.recipes.model.recipe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Ingredient extends RealmObject
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("elements")
    @Expose
    private RealmList<Element> elements;

    /**
     * @return The id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return The elements
     */
    public List<Element> getElements()
    {
        return elements;
    }

    /**
     * @param elements The elements
     */
    public void setElements(RealmList<Element> elements)
    {
        this.elements = elements;
    }

}
