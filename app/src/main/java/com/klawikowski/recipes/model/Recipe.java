
package com.klawikowski.recipes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.klawikowski.recipes.model.recipe.Image;
import com.klawikowski.recipes.model.recipe.Ingredient;
import com.klawikowski.recipes.model.recipe.Step;
import com.klawikowski.recipes.model.recipe.Tag;
import com.klawikowski.recipes.model.recipe.User;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class Recipe extends RealmObject
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("basicPortionNumber")
    @Expose
    private Integer basicPortionNumber;
    @SerializedName("preparationTime")
    @Expose
    private Integer preparationTime;
    @SerializedName("numberOfComments")
    @Expose
    private Integer numberOfComments;
    @SerializedName("numberOfLikes")
    @Expose
    private Integer numberOfLikes;
    @SerializedName("publishedAt")
    @Expose
    private String publishedAt;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @Ignore
    @SerializedName("embedTitle")
    @Expose
    private Object embedTitle;
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("ingredients")
    @Expose
    private RealmList<Ingredient> ingredients = new RealmList<Ingredient>();
    @SerializedName("tags")
    @Expose
    private RealmList<Tag> tags = new RealmList<Tag>();
    @SerializedName("steps")
    @Expose
    private RealmList<Step> steps = new RealmList<Step>();
    @SerializedName("images")
    @Expose
    private RealmList<Image> images = new RealmList<Image>();
    @Ignore
    @SerializedName("links")
    @Expose
    private Object links;

    /**
     * @return The title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return The basicPortionNumber
     */
    public Integer getBasicPortionNumber()
    {
        return basicPortionNumber;
    }

    /**
     * @param basicPortionNumber The basicPortionNumber
     */
    public void setBasicPortionNumber(Integer basicPortionNumber)
    {
        this.basicPortionNumber = basicPortionNumber;
    }

    /**
     * @return The preparationTime
     */
    public Integer getPreparationTime()
    {
        return preparationTime;
    }

    /**
     * @param preparationTime The preparationTime
     */
    public void setPreparationTime(Integer preparationTime)
    {
        this.preparationTime = preparationTime;
    }

    /**
     * @return The numberOfComments
     */
    public Integer getNumberOfComments()
    {
        return numberOfComments;
    }

    /**
     * @param numberOfComments The numberOfComments
     */
    public void setNumberOfComments(Integer numberOfComments)
    {
        this.numberOfComments = numberOfComments;
    }

    /**
     * @return The numberOfLikes
     */
    public Integer getNumberOfLikes()
    {
        return numberOfLikes;
    }

    /**
     * @param numberOfLikes The numberOfLikes
     */
    public void setNumberOfLikes(Integer numberOfLikes)
    {
        this.numberOfLikes = numberOfLikes;
    }

    /**
     * @return The publishedAt
     */
    public String getPublishedAt()
    {
        return publishedAt;
    }

    /**
     * @param publishedAt The publishedAt
     */
    public void setPublishedAt(String publishedAt)
    {
        this.publishedAt = publishedAt;
    }

    /**
     * @return The status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt()
    {
        return updatedAt;
    }

    /**
     * @param updatedAt The updatedAt
     */
    public void setUpdatedAt(String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The embedTitle
     */
    public Object getEmbedTitle()
    {
        return embedTitle;
    }

    /**
     * @param embedTitle The embedTitle
     */
    public void setEmbedTitle(Object embedTitle)
    {
        this.embedTitle = embedTitle;
    }

    /**
     * @return The id
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * @return The user
     */
    public User getUser()
    {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * @return The ingredients
     */
    public List<Ingredient> getIngredients()
    {
        return ingredients;
    }

    /**
     * @param ingredients The ingredients
     */
    public void setIngredients(RealmList<Ingredient> ingredients)
    {
        this.ingredients = ingredients;
    }

    /**
     * @return The tags
     */
    public List<Tag> getTags()
    {
        return tags;
    }

    /**
     * @param tags The tags
     */
    public void setTags(RealmList<Tag> tags)
    {
        this.tags = tags;
    }

    /**
     * @return The steps
     */
    public List<Step> getSteps()
    {
        return steps;
    }

    /**
     * @param steps The steps
     */
    public void setSteps(RealmList<Step> steps)
    {
        this.steps = steps;
    }

    /**
     * @return The images
     */
    public List<Image> getImages()
    {
        return images;
    }

    /**
     * @param images The images
     */
    public void setImages(RealmList<Image> images)
    {
        this.images = images;
    }

    /**
     * @return The links
     */
    public Object getLinks()
    {
        return links;
    }

    /**
     * @param links The links
     */
    public void setLinks(Object links)
    {
        this.links = links;
    }

}
