package com.klawikowski.recipes;

import android.app.Application;
import android.content.Context;

import com.klawikowski.recipes.db.IManagerData;
import com.klawikowski.recipes.db.ManagerData;
import com.klawikowski.recipes.ws.IWSCalls;
import com.klawikowski.recipes.ws.ManagerWS;
import com.squareup.leakcanary.LeakCanary;

/**
 * Main Application class
 */
public class RecipesApplication extends Application
{
    @Override
    public void onCreate()
    {
        super.onCreate();
        LeakCanary.install(this);
    }

    /**
     * Returns instance of ManagerWS
     *
     * @return ManagerWS singleton instance
     */
    public static IWSCalls getManagerWs()
    {
        return ManagerWS.getInstance();
    }

    /**
     * Returns manager data
     *
     * @param pContext Context
     * @return ManagerData singleton instance
     */
    public static IManagerData getManagerData(Context pContext)
    {
        return ManagerData.getInstance(pContext);
    }
}
