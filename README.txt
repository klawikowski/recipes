MIN API - 21
TARTGET API - 23

USED LIBRARIES
Realm https://realm.io/
Retrofit http://square.github.io/retrofit/
OkHttpClient http://square.github.io/okhttp/
OkHttpUrlConnection http://square.github.io/okhttp/
Glide https://github.com/bumptech/glide
OttoEvent Bus http://square.github.io/otto/
LeakCanary https://github.com/square/leakcanary
CardView
RecyclerView
AppCompat
SearchView

COMMENT
Simple application with list view and details activity.
Full CRUD database powered by Retrofit.
REST Webservice fetch powered by Retrofit.
Connection status and UI visual when no connection is available.
List recipe trasition to details activity view.
Paginated list load from ws.





